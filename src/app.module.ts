import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';

import { UsersModule } from './users/users.module';
import { SchedulingModule } from './scheduling/scheduling.module';
import { HookbinModule } from './rest-template/hookbin/hookbin.module';

@Module({
  imports: [ ConfigModule.forRoot({
                isGlobal: true,
              }),
              ScheduleModule.forRoot(),
              UsersModule,
              SchedulingModule, 
              HookbinModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
