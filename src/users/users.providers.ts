import { Connection } from 'mongoose';
import { UserSchema } from './schema/user.schema';

export const usersProviders = [
  {
    provide: 'USER_REPOSITORY',
    useFactory: (connection: Connection) => connection.model("User", UserSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];