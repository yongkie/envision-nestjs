import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './schema/users.interface';

@Injectable()
export class UsersRepository {
  
  constructor(
    @Inject('USER_REPOSITORY')
    private userRepository: Model<User>,
  ) {}

  async findAll(): Promise<User[]> {
    return this.userRepository.find().exec();
  }

  async findDOB(): Promise<User[]> {
    return this.userRepository.aggregate([{ $match:{ isSend:false , $expr: {
      $and: [
        { $eq: [{ $dayOfMonth: '$birthdate' }, { $dayOfMonth: new Date() }] },
        { $eq: [{ $month: '$birthdate' }, { $month: new Date() }] },
      ],
    },}}]).exec();
  }

  async findById(id: string): Promise<User> {
    const user = await this.userRepository.findById(id).exec();

    return user;
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = new this.userRepository(createUserDto);
    await user.save();
    return user;
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<void> {
    await this.userRepository.findByIdAndUpdate(id, updateUserDto);
  }

  async updateMany(): Promise<void> {
    await this.userRepository.updateMany({isSend : false});
  }

  async setSendStatus(id: string, isSend: boolean, updateUserDto: UpdateUserDto): Promise<void> {
    updateUserDto.isSend = isSend;
    await this.userRepository.findByIdAndUpdate(id, updateUserDto);
  }

  async delete(id: string): Promise<void> {
    await this.userRepository.findByIdAndDelete(id);
  }
}