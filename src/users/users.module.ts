import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database.module';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { UsersRepository } from './users.repository';
import { usersProviders } from './users.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [UsersController],
  providers: [...usersProviders, UsersRepository, UsersService]
})
export class UsersModule {}
