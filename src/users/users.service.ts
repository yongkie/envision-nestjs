import { Injectable } from '@nestjs/common';
import { UsersRepository } from './users.repository';
import { User } from './schema/users.interface';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  constructor(private readonly userRepository: UsersRepository) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    return this.userRepository.create(createUserDto);
  }

  async findAll(): Promise<User[]> {
    return this.userRepository.findAll();
  }

  async findDOB(): Promise<User[]> {
    return this.userRepository.findDOB();
  }

  async findOne(id: string): Promise<User | null>  {
    return this.userRepository.findById(id);
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<void> {
    return this.userRepository.update(id, updateUserDto);
  }

  async updateMany(): Promise<void> {
    return this.userRepository.updateMany();
  }

  async setSendStatus(id: string, isSend: boolean, updateUserDto: UpdateUserDto): Promise<void> {
    return this.userRepository.setSendStatus(id, isSend, updateUserDto);
  }

  async remove(id: string): Promise<void>{
    return this.userRepository.delete(id);
  }
}
