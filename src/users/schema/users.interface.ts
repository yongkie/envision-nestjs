import { Document } from 'mongoose';

export interface User extends Document {
  readonly id: string;
  readonly firstname: string;
  readonly lastname: string;
  birthdate: Date;
  readonly location: string;
  readonly isSend: boolean;
}