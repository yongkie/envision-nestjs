import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  id: String,
  firstname: String,
  lastname: String,
  birthdate: { type: Date, default: Date.now() + 7 },
  location: String,
  isSend: {type: Boolean, default: false},
});