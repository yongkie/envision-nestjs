import { IsNotEmpty, IsString, IsDate, isBoolean } from "class-validator";

export class CreateUserDto {
    @IsString()
    @IsNotEmpty()
    first_name: string;

    @IsString()
    @IsNotEmpty()
    last_name: string;

    @IsDate()
    @IsNotEmpty()
    birthdate: Date;

    @IsString()
    @IsNotEmpty()
    location: string;

    @IsNotEmpty()
    isSend: boolean;
}
