import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { HookbinService } from 'src/rest-template/hookbin/hookbin.service';
import { Cron, CronExpression } from '@nestjs/schedule';
import { User } from 'src/users/schema/users.interface';

@Injectable()
export class SchedulingService {

    constructor(private readonly usersService: UsersService, 
                private readonly hookbinService: HookbinService) {}

    private readonly logger = new Logger(SchedulingService.name);

    @Cron('*/30 * 9-17 * * *')
    async testCron(){
        this.logger.debug('Called every 30 Seconds');
        const users:User[] = await this.usersService.findDOB();
        for (let i = 0; i < users.length; i++) {
            const fullName = users[i].firstname + " " + users[i].lastname;
            //console.log(users[i]);
            this.logger.debug('Name : ' + fullName);
            const response = this.hookbinService.callHookbin(fullName);
            response.subscribe((res) => {
                console.log(res);
                if(res['success'] == true){
                    this.usersService.setSendStatus(users[i]._id, true, users[i]);
                }
            });
        }
    }

    @Cron('59 59 23 31 12 *')
    async resetGreeting(){
        this.logger.debug('Reset Send Status to False for a New Year');
        this.usersService.updateMany();
    }
}
