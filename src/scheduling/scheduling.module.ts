import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database.module';
import { UsersService } from 'src/users/users.service';
import { UsersRepository } from 'src/users/users.repository';
import { usersProviders } from 'src/users/users.providers';
import { SchedulingService } from './scheduling.service';
import { HookbinService } from 'src/rest-template/hookbin/hookbin.service';
import { HttpModule } from '@nestjs/axios';

@Module({
    imports: [DatabaseModule, HttpModule],
    providers: [...usersProviders, UsersRepository, UsersService, HookbinService, SchedulingService]
})
export class SchedulingModule {}
