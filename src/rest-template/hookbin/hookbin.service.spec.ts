import { Test, TestingModule } from '@nestjs/testing';
import { HookbinService } from './hookbin.service';

describe('HookbinService', () => {
  let service: HookbinService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HookbinService],
    }).compile();

    service = module.get<HookbinService>(HookbinService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
