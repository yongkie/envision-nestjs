import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { HookbinService } from './hookbin.service';

@Module({
  imports: [HttpModule.register({
    timeout: 10000,
    maxRedirects: 5,
  })],
  providers: [HookbinService]
})
export class HookbinModule {}
