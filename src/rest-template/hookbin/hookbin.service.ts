import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { AxiosResponse } from 'axios';
import { map, Observable } from 'rxjs';

@Injectable()
export class HookbinService {
    constructor(private httpService: HttpService) {}

    private readonly logger = new Logger(HookbinService.name);

     callHookbin(name: string): Observable<AxiosResponse<any>>{
        const data = JSON.stringify({
            greeting: "Hey, " + name + " it's your birthday"
        })

        const response =  this.httpService.post(process.env.HOOKBIN_URL, data,
                {
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': data.length.toString()
                },
                }).pipe(map((res) => {
                    return res.data;
                  }));

        return response;        
        
    }
}
